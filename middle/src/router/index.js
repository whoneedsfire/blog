import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  // 一级路由
  { path: '/login', component: () => import('@/views/Login') },
  {
    path: '/',
    redirect: '/dashboard',
    component: () => import('@/views/Home'),
    children: [
      {
        path: 'messageboard',
        component: () => import('@/views/Home/MessageBoard'),
      },
      { path: 'dashboard', component: () => import('@/views/Home/Dashboard') },
      {
        path: 'user',
        component: () => import('@/views/Home/User'),
      },

      { path: 'dashboard', component: () => import('@/views/Home/Dashboard') },
      { path: 'user', component: () => import('@/views/Home/User') },
      // {
      //   path: 'user/add',
      //   component: () => import('@/views/Home/User/UserAdd'),
      // },
      { path: 'post', component: () => import('@/views/Home/Post') },
      {
        path: 'post/article/list',
        component: () => import('@/views/Home/Post/List'),
      },
      {
        path: 'post/detail',
        component: () => import('@/views/Home/Post/Detail'),
      },
      {
        path: 'post/edit',
        component: () => import('@/views/Home/Post/Edit'),
      },
      {
        path: 'post/sort',
        component: () => import('@/views/Home/Post/Sort'),
      },
    ],
  },
  { path: '*', component: () => import('@/views/404') },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});
//添加导航守卫
// 设置全局前置导航守卫
/* router.beforeEach((to, from, next) => {
  // to: 需要访问的路径
  // from: 从哪个路径来的
  // next: 放行
  let token = localStorage.getItem('token');
  if (token) {
    if (to.path == '/login') {
      next('/');
    } else {
      next();
    }
  } else {
    if (to.path != '/login') {
      next('/login');
    } else {
      next();
    }
  }
}); */
export default router;
