import Vue from 'vue'
import App from './App.vue'
import router from './router'

// 配置iconpark
import { install } from '@icon-park/vue/es/all'
import '@icon-park/vue/styles/index.css'
install(Vue)

import './plugins/element.js'
import './plugins/xzdAxios.js'
//eacharts引入
import * as echarts from 'echarts'
import VueParticles from 'vue-particles'
// import './plugins/testPlugin.js'
// import axios from '@/request'
// Vue.prototype.$http = axios

// markdown
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'

Vue.use(mavonEditor)
Vue.use(VueParticles)
Vue.config.productionTip = false

Vue.prototype.$echarts = echarts

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app')
