// 一. 导入包 Vue , axios的实例对象
import Vue from 'vue'
import axios from '../request'

// 二. 定义插件对象
const xzdAxios = {
  install(Vue) {
    Vue.prototype.$http = axios
  },
}

// 三. Vue.use() 注册插件
Vue.use(xzdAxios)
