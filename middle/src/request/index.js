// 一. 导入axios包
import axios from 'axios'

// 二. 实例化对象
const instance = axios.create({
  baseURL: 'http://122.112.251.104:3000',
  timeout: 3000, // 单位 毫秒
})

// 三. 配置拦截器
// 添加请求拦截器，在请求头中加token
instance.interceptors.request.use((config) => {
  const token = localStorage.getItem('token')
  if (token) {
    // 如果 token 存在，在每个 HTTP header 都加上 token
    // Bearer 是 JWT 的认证头部信息
    config.headers.common['Authorization'] = `Bearer ${token}`
  }
  return config
})
//添加响应拦截器
instance.interceptors.response.use(
  // 成功的回调
  (response) => {
    return response.data
    // return response
  },
  // 失败的回调
  (error) => {
    return Promise.reject(error)
  }
)

// 四. 导出对象
export default instance
