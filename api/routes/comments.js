var express = require('express');
var router = express.Router();

const { getAll, getOne, exec } = require('../db');

/* /article */
//新增   发表文章
router.post('/', async function (req, res) {
  // console.log(req);
  // console.log(req.body.params);
  // console.log(req.body);
  // console.log(req.body.params);
  const { comments, articleId } = req.body.params;
  // console.log(comments, articleId);
  //编写sql语句

  let sql = `insert into comments (content, article_id) values ('${comments}','${articleId}')`;
  //执行sql语句,返回数据
  const data = await exec(sql);

  // console.log(data);
  res.send({
    code: 0,
    message: '发表成功',
    result: data,
  });
});

router.get('/', async (req, res) => {
  // console.log(req.query);
  const { article_id } = req.query;
  // console.log(article_id);
  let sql1 = `select count(*) as totalCom from comments`;
  let { totalCom } = await getOne(sql1);

  // console.log(totalCom);
  let sql = `select * from comments where article_id=${article_id} `;
  const data = await getAll(sql);
  console.log(data);
  res.send({
    code: 0,
    message: '获取成功',
    result: { data, totalCom },
  });
});

// 获取评论数
router.get('/total', async (req, res) => {
  let sql1 = `select count(*) as totalCom from comments`;
  let { totalCom } = await getOne(sql1);

  res.send({
    code: 0,
    message: '获取成功',
    result: totalCom,
  });
});

module.exports = router;
