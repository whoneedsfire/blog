//一 导入express包
var express = require('express');

const { debug } = require('../config');
//导入db，操作数据库
const { getAll, getOne, exec } = require('../db');
//二 创建router路由对象
var router = express.Router();

/**
 * get 获取
 */
router.get('/', async (req, res) => {
  //解析请求参数
  //编写sql语句
  let sql = `SELECT * FROM leave_words;`;
  const data = await getAll(sql);
  let sqlWord = `select count(*) as words from leave_words `;
  let { words } = await getOne(sqlWord);
  // console.log(world);

  res.send({
    code: 0,
    message: '获取所有待办成功',
    result: { data, words },
  });
});

/* GET leave_words  */
router.get('/page', async (req, res) => {
  // 刷新页面使所有文章未选中
  let sqlllw = `update leave_words set checked=0 `;
  await exec(sqlllw);
  //解析请求参数
  const { page = 1, size = 5 } = req.query;
  let sql = `select count(*) as total from leave_words `;
  let { total } = await getOne(sql);

  //偏移量
  let offset = (page - 1) * size;

  sql = `select *from leave_words limit ${offset},${size}`;
  const data = await getAll(sql);
  res.send({
    code: 0,
    message: '获取用户成功',
    result: {
      total,
      data,
    },
  });
});

/**
 * get 模糊查询 like
 */
router.get('/select', async (req, res) => {
  //结构请求参数
  const { keywords } = req.query;
  console.log(keywords);
  //操作数据库
  const { page = 1, size = 5 } = req.query;
  let sql = `select count(*) as total from leave_words where name LIKE '%${keywords}%' OR content LIKE '%${keywords}%' `;
  let { total } = await getOne(sql);

  //偏移量
  let offset = (page - 1) * size;

  sql = `SELECT  * FROM leave_words where name LIKE '%${keywords}%' OR content LIKE '%${keywords}%' limit ${offset},${size}`;
  const data = await getAll(sql);
  res.send({
    code: 0,
    message: '查询成功',
    result: {
      data,
      total,
    },
  });
});

/**
 * post 新增
 */
router.post('/', async (req, res) => {
  const { name, content } = req.body;
  let sql = `insert into  leave_words (name,content) values ('${name}','${content}')`;
  const { insertId } = await exec(sql);
  res.send({
    code: 0,
    message: '添加用户成功',
    result: {
      id: insertId,
      name,
      content,
    },
  });
});

/**
 * delete
 */
router.delete('/:id', async (req, res) => {
  const { id } = req.params;
  //操作数据库
  let sql = `delete from leave_words where id=${id}`;
  await exec(sql);
  res.send({
    code: 0,
    message: '删除成功',
    result: '',
  });
});
router.get('/list', async (req, res) => {
  const { page = 1, size = 10, arr } = req.query;
  // 将传来的数组对象转为数字类型
  let arr1 = [];
  arr.forEach((row) => {
    let d = parseInt(row);
    arr1.push(d);
  });
  // 将id等于数组中的值的checked赋为1
  let sqllll = `update leave_words set checked=1 where id in(${arr1})`;
  await exec(sqllll);
  // 将id不等于数组中的值的checked赋为0
  let sqlllll = `update leave_words set checked=0 where id not in(${arr1})`;
  await exec(sqlllll);

  let sqll = `select count(*) as total from leave_words`;

  let { total } = await getOne(sqll);

  let offset = (page - 1) * size;

  let sql = `select * from leave_words limit ${offset},${size}`;
  const data = await getAll(sql);

  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  });
});

router.get('/delall', async (req, res) => {
  const { page = 1, size = 5 } = req.query;
  // 将id等于数组中的值的checked赋为1
  let sqlll = `delete from leave_words where checked=1 `;
  await exec(sqlll);
  // 选择类型
  let sqll = `select count(*) as total from leave_words `;

  let { total } = await getOne(sqll);

  let offset = (page - 1) * size;

  let sql = `select * from leave_words limit ${offset},${size}`;
  const data = await getAll(sql);

  res.send({
    code: 0,
    message: '删除成功',
    result: { total, data },
  });
});
//四 导出
module.exports = router;
