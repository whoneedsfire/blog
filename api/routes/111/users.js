var express = require('express')
var router = express.Router()
// 导入db, 操作数据库
const { getAll, getOne, exec } = require('../db')
/* GET users  */
router.get('/', async (req, res) => {
  // 刷新页面使所有管理员未选中
  let sqlllw = `update articles set checked=0 `
  await exec(sqlllw)
  let sql = `update users set checked=0 `
  await exec(sql)
  //解析请求参数
  const { page = 1, size = 5 } = req.query
  sql = `select count(*) as total from users`
  let { total } = await getOne(sql)
  //偏移量
  let offset = (page - 1) * size
  //操作数据库
  sql = `select id, username ,user_type from users limit ${offset},${size}`
  const data = await getAll(sql)
  res.send({
    code: 0,
    message: '获取用户成功',
    result: {
      total,
      data,
    },
  })
})

// GET /users/:id
router.get('/:id', async (req, res) => {
  const { id } = req.params

  let sql = `select * from users where id=${id}`
  const data = await getOne(sql)

  res.send({
    code: 0,
    message: '获取单个用户成功',
    result: data,
  })
})
/**
 * post
 */
router.post('/', async (req, res) => {
  const { username, password } = req.body
  let sql = `insert into  users (username,password) values ('${username}','${password}')`
  const { insertId } = await exec(sql)
  res.send({
    code: 0,
    message: '添加用户成功',
    result: {
      id: insertId,
      username,
      password,
    },
  })
})

// PUT /users/id {username: 'new', password: 'new'}
router.put('/:id', async (req, res) => {
  const { id } = req.params
  const { username, password } = req.body

  let sql = `update users set username='${username}', password='${password}' where id=${id}`
  await exec(sql)

  res.send({
    code: 0,
    message: '修改成功',
    result: {
      id,
      username,
      password,
    },
  })
})
router.get('/list', async (req, res) => {
  const { page = 1, size = 10, arr } = req.query
  // 将传来的数组对象转为数字类型
  let arr1 = []
  arr.forEach((row) => {
    let d = parseInt(row)
    arr1.push(d)
  })
  // 将id等于数组中的值的checked赋为1
  let sqllll = `update users set checked=1 where id in(${arr1})`
  await exec(sqllll)
  // 将id不等于数组中的值的checked赋为0
  let sqlllll = `update users set checked=0 where id not in(${arr1})`
  await exec(sqlllll)

  let sqll = `select count(*) as total from users`

  let { total } = await getOne(sqll)

  let offset = (page - 1) * size

  let sql = `select * from users limit ${offset},${size}`
  const data = await getAll(sql)

  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  })
})

router.get('/delall', async (req, res) => {
  const { page = 1, size = 5 } = req.query
  // 将id等于数组中的值的checked赋为1
  let sqlll = `delete from users where checked=1 `
  await exec(sqlll)
  // 选择类型
  let sqll = `select count(*) as total from users `

  let { total } = await getOne(sqll)

  let offset = (page - 1) * size

  let sql = `select * from users limit ${offset},${size}`
  const data = await getAll(sql)

  res.send({
    code: 0,
    message: '删除成功',
    result: { total, data },
  })
})
/**
 * delete
 */
router.delete('/:id', async (req, res) => {
  const { id } = req.params
  //操作数据库
  let sql = `delete from users where id=${id}`
  await exec(sql)
  res.send({
    code: 0,
    message: '删除成功',
    result: '',
  })
})

module.exports = router
