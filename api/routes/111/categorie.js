var express = require('express');
var router = express.Router();

const { getAll, getOne, exec } = require('../db');

/* GET  */
router.get('/', async (req, res) => {
  let sql = `select * from categories`;
  const data = await getAll(sql);
  res.send({
    code: 0,
    message: '获取成功',
    result: data,
  });
});

module.exports = router;
