var express = require('express');
var router = express.Router();

const { getAll, getOne, exec } = require('../db');

/* GET /article */
router.get('/', async (req, res) => {
  const { page = 1, size = 10, arr } = req.query;
  // 将传来的数组对象转为数字类型
  let arr1 = [];
  arr.forEach((row) => {
    let d = parseInt(row);
    arr1.push(d);
  });
  // 将id等于数组中的值的checked赋为1
  let sqllll = `update articles set checked=1 where id in(${arr1})`;
  await exec(sqllll);
  // 将id不等于数组中的值的checked赋为0
  let sqlllll = `update articles set checked=0 where id not in(${arr1})`;
  await exec(sqlllll);

  let sqll = `select count(*) as total from articles`;

  let { total } = await getOne(sqll);

  let offset = (page - 1) * size;

  let sql = `select * from articles limit ${offset},${size}`;
  const data = await getAll(sql);

  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  });
});

/**
 * get 模糊查询 like
 */
router.get('/select', async (req, res) => {
  //console.log(req.query)
  //结构参数
  const { keywords } = req.query;
  const { page = 1, size = 5 } = req.query; // 操作数据库
  let sql = `select count(*) as total from articles where CONCAT(title,author,abstract) LIKE '%${keywords}%' `;
  let { total } = await getOne(sql); //偏移量
  let offset = (page - 1) * size;
  sql = `SELECT  * FROM articles where CONCAT(title,author,abstract) LIKE '%${keywords}%' limit ${offset},${size}`;
  const data = await getAll(sql);
  res.send({
    code: 0,
    message: '查询成功',
    result: {
      data,
      total,
    },
  });
});

module.exports = router;
