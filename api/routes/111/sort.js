var express = require('express');
var router = express.Router();
const { getAll, getOne, exec } = require('../db');

/* GET /sort 按分类abstract获取*/
router.get('/', async (req, res) => {
  const { page = 1, size = 5, abstract, arr } = req.query;
  // 刷新页面使所有文章未选中
  let sqlllw = `update articles set checked=0 `;
  await exec(sqlllw);

  if (arr) {
    // 将传来的数组对象转为数字类型
    let arr1 = [];
    arr.forEach((row) => {
      let d = parseInt(row);
      arr1.push(d);
    });
    // 将id等于数组中的值的checked赋为1
    let sqllll = `update articles set checked=1 where id in(${arr1})`;
    await exec(sqllll);
    // 将id不等于数组中的值的checked赋为0
    let sqlllll = `update articles set checked=0 where id not in(${arr1})`;
    await exec(sqlllll);
    console.log(arr);
  }

  // 选择类型
  let sqll = `select count(*) as total from articles where abstract='${abstract}'`;

  let { total } = await getOne(sqll);

  let offset = (page - 1) * size;

  let sql = `select * from articles where abstract='${abstract}' limit ${offset},${size}`;
  const data = await getAll(sql);
  // console.log(data);
  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  });
});

module.exports = router;
