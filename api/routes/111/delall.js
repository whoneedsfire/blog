var express = require('express');
var router = express.Router();

const { getAll, getOne, exec } = require('../db');

/* GET /article */
router.get('/', async (req, res) => {
  const { page = 1, size = 5, abstract } = req.query;
  // 将id等于数组中的值的checked赋为1
  let sqlll = `delete from articles where checked=1 `;
  await exec(sqlll);
  // 选择类型
  let sqll = `select count(*) as total from articles where abstract='${abstract}'`;

  let { total } = await getOne(sqll);

  let offset = (page - 1) * size;

  let sql = `select * from articles where abstract='${abstract}' limit ${offset},${size}`;
  const data = await getAll(sql);

  res.send({
    code: 0,
    message: '删除成功',
    result: { total, data },
  });
});

module.exports = router;
