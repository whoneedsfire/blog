const { query } = require('express');
var express = require('express');
var router = express.Router();

const { getAll, getOne, exec } = require('../db');

/* GET /article */
// 在 created 和 mounted 阶段加载页面，设置未选中
router.get('/', async (req, res) => {
  // 刷新页面使所有文章未选中
  let sqlllw = `update articles set checked=0 `;
  await exec(sqlllw);

  const { page = 1, size = 5 } = req.query;
  let sql = `select count(*) as total from articles`;
  let sqlClick = `select sum(clicks) as clicks from articles`;
  let sqlGood = `select sum(good) as good from articles`;
  let { total } = await getOne(sql);
  let { clicks } = await getOne(sqlClick);
  let { good } = await getOne(sqlGood);
  // console.log(total, clicks, good);
  let offset = (page - 1) * size;
  sql = `select * from articles limit ${offset},${size}`;
  const data = await getAll(sql);
  res.send({
    code: 0,
    message: '获取成功',
    result: { total, clicks, good, data },
  });
});

/* GET /article/list */
// 文章列表界面 选中 和 未选中 状态接口
router.get('/list', async (req, res) => {
  const { page = 1, size = 10, arr } = req.query;
  // 将传来的数组对象转为数字类型
  let arr1 = [];
  arr.forEach((row) => {
    let d = parseInt(row);
    arr1.push(d);
  });
  // 将id等于数组中的值的checked赋为1
  let sqllll = `update articles set checked=1 where id in(${arr1})`;
  await exec(sqllll);
  // 将id不等于数组中的值的checked赋为0
  let sqlllll = `update articles set checked=0 where id not in(${arr1})`;
  await exec(sqlllll);

  let sqll = `select count(*) as total from articles`;

  let { total } = await getOne(sqll);

  let offset = (page - 1) * size;

  let sql = `select * from articles limit ${offset},${size}`;
  const data = await getAll(sql);

  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  });
});

/* GET /article/sort 按分类abstract获取*/
router.get('/sort', async (req, res) => {
  const { page = 1, size = 5, abstract, arr } = req.query;

  // 刷新页面使所有文章未选中
  let sqlllw = `update articles set checked=0`;
  await exec(sqlllw);

  if (arr) {
    // 将传来的数组对象转为数字类型
    let arr1 = [];
    arr.forEach((row) => {
      let d = parseInt(row);
      arr1.push(d);
    });
    // 将id等于数组中的值的checked赋为1
    let sqllll = `update articles set checked=1 where id in(${arr1}) and abstract='${abstract}'`;
    await exec(sqllll);
    // 将id不等于数组中的值的checked赋为0
    let sqlllll = `update articles set checked=0 where id not in(${arr1}) and abstract='${abstract}' `;
    await exec(sqlllll);
  }
  // 选择类型
  let sqll = `select count(*) as total from articles where abstract='${abstract}'`;
  // 计算对应类型的总文章数
  let { total } = await getOne(sqll);

  let offset = (page - 1) * size;
  // 根据limit筛选每一类的文章数
  let sql = `select * from articles where abstract='${abstract}' limit ${offset},${size}`;
  const data = await getAll(sql);
  // console.log(data);
  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  });
});

/* GET /article/select  根据关键字模糊查询 */
router.get('/select', async (req, res) => {
  // console.log(req.query);
  //结构请求参数
  const { keywords, page = 1, size = 10 } = req.query;

  //操作数据库
  let sql = `select count(*) as total from articles where title LIKE '%${keywords}%' OR content LIKE '%${keywords}%' `;
  let { total } = await getOne(sql);
  // //偏移量
  let offset = (page - 1) * size;

  sql = `SELECT  * FROM articles where title LIKE '%${keywords}%' OR content LIKE '%${keywords}%' limit ${offset},${size}`;
  const data = await getAll(sql);
  res.send({
    code: 0,
    message: '查询成功',
    result: {
      data,
      total,
    },
  });
});
/* GET /article/categorie */
//在sort.vue和list.vue中获取分类
router.get('/categorie', async (req, res) => {
  let sql = `select * from categories`;
  const data = await getAll(sql);
  res.send({
    code: 0,
    message: '获取成功',
    result: data,
  });
});

/* GET /article/sortDelall */
// 在sort.vue中多选删除
router.get('/sortDelall', async (req, res) => {
  const { page = 1, size = 5, abstract } = req.query;
  // 将id等于数组中的值的checked赋为1
  let sqlll = `delete from articles where checked=1 `;
  await exec(sqlll);
  // 选择类型
  let sqll = `select count(*) as total from articles where abstract='${abstract}'`;

  let { total } = await getOne(sqll);

  let offset = (page - 1) * size;

  let sql = `select * from articles where abstract='${abstract}' limit ${offset},${size}`;
  const data = await getAll(sql);

  res.send({
    code: 0,
    message: '删除成功',
    result: { total, data },
  });
});

/* GET /article/delall */
// 在list.vue中多选删除
router.get('/delall', async (req, res) => {
  const { page = 1, size = 5 } = req.query;
  // 将id等于数组中的值的checked赋为1
  let sqlll = `delete from articles where checked=1 `;
  await exec(sqlll);
  // 选择类型
  let sqll = `select count(*) as total from articles`;

  let { total } = await getOne(sqll);

  let offset = (page - 1) * size;

  let sql = `select * from articles limit ${offset},${size}`;
  const data = await getAll(sql);

  res.send({
    code: 0,
    message: '删除成功',
    result: { total, data },
  });
});

// 获取一个    article/${id}
// sort.vue和list.vue中的编辑和删除以及detail.vue
router.get('/detail/:id', async (req, res) => {
  const { id } = req.params;
  if (req.query.good) {
    const { good } = req.query;
    let goods = parseInt(good);
    let sql1 = `update articles set good = ${goods} where id = ${id}`;
    await getOne(sql1);
  }
  let sql2 = `select * from articles  where id = ${id}`;
  const data = await getOne(sql2);

  res.send({
    code: 0,
    message: '获取成功',
    result: data,
  });
});
// 中台获取回显
/* router.get('/:id', async (req, res) => {
  const { id } = req.params;

  if (req.query.good) {
    const { good } = req.query;
    let goods = parseInt(good);
    let sql1 = `update articles set good = ${goods} where id = ${id}`;
    await getOne(sql1);
  }

  let sql2 = `select * from articles  where id = ${id}`;
  const data = await getOne(sql2);

  res.send({
    code: 0,
    message: '获取成功',
    result: data,
  });
}); */
/* /article */
//新增   发表文章
router.post('/', async function (req, res) {
  // console.log(req);
  // console.log(req.body.params.content);
  console.log(req.body.params);
  const {
    title,
    author,
    abstract,
    content,
    click,
    category_id,
    image_address,
  } = req.body.params;
  //编写sql语句
  let sql = `insert into articles (title, author, abstract, content, clicks, category_id,image_address) values ('${title}','${author}','${abstract}','${content}',${click},${category_id},'${image_address}')`;
  //执行sql语句,返回数据
  const data = await exec(sql);
  console.log(data);
  res.send({
    code: 0,
    message: '发表成功',
    result: data,
  });
});

// 修改选中值
// PUT /users/id {username: 'new', password: 'new'}
/* router.put('/:id', async (req, res) => {
  console.log(req.params.id);
  // const { id } = req.params;
  // const { checked } = req.body;

  let sql = `update articles set checked=1 where id=${req.params.id}`;
  await exec(sql);
  let sqll = `update articles set checked=0 where id!=${req.params.id}`;
  await exec(sqll);
  res.send({
    code: 0,
  });
}); */

// 删除单个
// sort.vue和list.vue中单个删除
router.delete('/:id', async (req, res) => {
  // 一. 解析请求参数
  const { id } = req.params;
  // 二. 操作数据库
  let sql = `delete from articles where id=${id}`;
  await exec(sql);
  // 三. 返回结果
  res.send({
    code: 0,
    message: '删除成功',
    result: '',
  });
});

/**
 * get 模糊查询 like
 */
router.get('/select', async (req, res) => {
  // console.log(req.query);
  //结构参数
  const { keywords } = req.query;
  const { page = 1, size = 5 } = req.query; // 操作数据库
  let sql = `select count(*) as total from articles where CONCAT(title,author,abstract) LIKE '%${keywords}%' `;
  let { total } = await getOne(sql); //偏移量
  let offset = (page - 1) * size;
  sql = `SELECT * FROM articles where CONCAT(title,author,abstract) LIKE '%${keywords}%' limit ${offset},${size}`;
  const data = await getAll(sql);
  res.send({
    code: 0,
    message: '查询成功',
    result: {
      data,
      total,
    },
  });
});

router.get('/num', async (req, res) => {
  let sql = `select count(*) as total1 from articles`;
  const { total1 } = await getOne(sql);
  sql = `select count(*) as total2 from categories`;
  const { total2 } = await getOne(sql);
  console.log(total1);
  console.log(total2);
  res.send({
    code: 0,
    message: '获取数量成功',
    result: {
      total1,
      total2,
    },
  });
});

module.exports = router;
