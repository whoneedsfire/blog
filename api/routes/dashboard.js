//一 导入express包
var express = require('express')

const { debug } = require('../config')
//导入db，操作数据库
const { getAll, getOne, exec } = require('../db')
//二 创建router路由对象
var router = express.Router()
router.get('/', async (req, res) => {
  //解析请求参数
  //编写sql语句
  let sql = `SELECT COUNT(category_id)as value,alias as name FROM articles,categories where category_id=categories.id GROUP BY category_id`
  const data = await getAll(sql)

  res.send({
    code: 0,
    message: '获取所有待办成功',
    result: data,
  })
})
router.get('/time', async (req, res) => {
  //解析请求参数
  //编写sql语句
  let sql = `SELECT MONTH(create_time) name,COUNT(*) value FROM articles GROUP BY name`
  const data = await getAll(sql)

  res.send({
    code: 0,
    message: '获取所有待办成功',
    result: data,
  })
})

//四 导出
module.exports = router
