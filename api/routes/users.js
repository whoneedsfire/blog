var express = require('express')
var router = express.Router()
// 导入db, 操作数据库
const { getAll, getOne, exec } = require('../db')
/* GET users  */
router.get('/', async (req, res) => {
  //解析请求参数
  const { page = 1, size = 5 } = req.query
  let sql = `select count(*) as total from users`
  let { total } = await getOne(sql)
  //偏移量
  let offset = (page - 1) * size
  //操作数据库
  sql = `select id, username ,Signature from users limit ${offset},${size}`
  const data = await getAll(sql)
  res.send({
    code: 0,
    message: '获取用户成功',
    result: {
      total,
      data,
    },
  })
})

router.get('/select', async (req, res) => {
  //结构请求参数
  const { keywords } = req.query
  console.log(keywords)
  //操作数据库
  const { page = 1, size = 5 } = req.query
  let sql = `select count(*) as total from users where username LIKE '%${keywords}%' `
  let { total } = await getOne(sql)

  // //偏移量
  let offset = (page - 1) * size

  sql = `SELECT  * FROM  users where username LIKE '%${keywords}%' limit ${offset},${size}`
  const data = await getAll(sql)
  res.send({
    code: 0,
    message: '查询成功',
    result: {
      data,
      total,
    },
  })
})

// GET /users/:id
router.get('/:id', async (req, res) => {
  const { id } = req.params

  let sql = `select * from users where id=${id}`
  const data = await getOne(sql)

  res.send({
    code: 0,
    message: '获取单个用户成功',
    result: data,
  })
})
/**
 * post
 */
router.post('/', async (req, res) => {
  //console.log(123)
  const { username, password, email, Signature } = req.body
  let sql = `insert into  users (username,password,email,Signature) values ('${username}','${password}','${email}','${Signature}')`
  const { insertId } = await exec(sql)
  res.send({
    code: 0,
    message: '添加用户成功',
    result: {
      id: insertId,
      username,
      password,
      email,
      Signature,
    },
  })
})

// PUT /users/id {username: 'new', password: 'new'}
router.put('/:id', async (req, res) => {
  const { id } = req.params
  const { username, password, email, Signature } = req.body

  let sql = `update users set username='${username}', password='${password}',email='${email}',Signature='${Signature}' where id=${id}`
  await exec(sql)

  res.send({
    code: 0,
    message: '修改成功',
    result: {
      id,
      username,
      password,
    },
  })
})
/**
 * delete
 */
router.delete('/:id', async (req, res) => {
  //console.log(123)
  // 一. 解析请求参数
  const { id } = req.params
  // 二. 操作数据库
  let sql = `delete from users where id=${id}`
  await exec(sql)
  // 三. 返回结果
  res.send({
    code: 0,
    message: '删除成功',
    result: '',
  })
})

/**
 * get 模糊查询 like
 */

module.exports = router
