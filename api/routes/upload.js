const express = require('express');
const path = require('path');
const fs = require('fs'); //访问本地文件系统的功能模块
const multer = require('multer');
const router = express.Router();

//图片上传
router.post(
  '/img',
  multer({
    //设置文件存储路径
    dest: 'upload/img',
  }).array('file', 1),
  (req, res) => {
    // console.log(req.files)
    let files = req.files;
    let file = files[0];
    let fileInfo = {};
    let path = 'upload/img/' + Date.now().toString() + '_' + file.originalname;
    console.log(path);
    fs.renameSync('./upload/img/' + file.filename, path);
    //获取文件基本信息
    fileInfo.type = file.mimetype;
    fileInfo.name = file.originalname;
    fileInfo.size = file.size;
    fileInfo.path = path;
    const url = `http://localhost:3000/` + path;
    // console.log('地址', url)
    res.send({
      code: 200,
      msg: 'OK',
      data: url,
    });
  }
);

router.post('/del', (req, res) => {
  console.log(req.body);
  const { url } = req.body;
  const arr = url.split('/');
  const arr2 = arr[arr.length - 1];

  fs.unlink(arr2, (err) => {
    if (err) {
      res.send({
        code: 500,
        message: '文件删除失败',
      });
      return;
    }
    res.send({
      code: 0,
      message: '文件删除成功',
    });
  });
});

module.exports = router;
