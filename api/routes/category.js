var express = require('express');
var router = express.Router();

const { getAll, getOne, exec } = require('../db');
/* GET /article/sort 按分类abstract获取*/
router.get('/html', async (req, res) => {
  // 选择类型
  let sqll = `select count(*) as total from articles where abstract='html'`;

  let { total } = await getOne(sqll);

  let sql = `select * from articles where abstract='html'`;
  const data = await getAll(sql);
  // console.log(data);
  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  });
});
router.get('/css', async (req, res) => {
  // 选择类型
  let sqll = `select count(*) as total from articles where abstract='css'`;

  let { total } = await getOne(sqll);

  let sql = `select * from articles where abstract='css'`;
  const data = await getAll(sql);
  // console.log(data);
  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  });
});
router.get('/javascript', async (req, res) => {
  // 选择类型
  let sqll = `select count(*) as total from articles where abstract='javascript'`;

  let { total } = await getOne(sqll);

  let sql = `select * from articles where abstract='javascript'`;
  const data = await getAll(sql);
  // console.log(data);
  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  });
});
router.get('/WebApi', async (req, res) => {
  // 选择类型
  let sqll = `select count(*) as total from articles where abstract='WebApi'`;

  let { total } = await getOne(sqll);

  let sql = `select * from articles where abstract='WebApi'`;
  const data = await getAll(sql);
  // console.log(data);
  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  });
});
router.get('/jquery', async (req, res) => {
  // 选择类型
  let sqll = `select count(*) as total from articles where abstract='jquery'`;

  let { total } = await getOne(sqll);

  let sql = `select * from articles where abstract='jquery'`;
  const data = await getAll(sql);
  // console.log(data);
  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  });
});
router.get('/mysql', async (req, res) => {
  // 选择类型
  let sqll = `select count(*) as total from articles where abstract='mysql'`;

  let { total } = await getOne(sqll);

  let sql = `select * from articles where abstract='mysql'`;
  const data = await getAll(sql);
  // console.log(data);
  res.send({
    code: 0,
    message: '获取成功',
    result: { total, data },
  });
});
module.exports = router;
