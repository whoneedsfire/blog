var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors');
// token导入
const AuthenticatePolicy = require('./policies/AuthenticatePolicy');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const todosRouter = require('./routes/todos');
const articleRouter = require('./routes/article');
const categoryRouter = require('./routes/category');
const commentsRouter = require('./routes/comments');
// const listRouter = require('./routes/list');
// const delallRouter = require('./routes/delall');
const dashRouter = require('./routes/dashboard');
const leavewordsRouter = require('./routes/leavewords');
const uploadRouter = require('./routes/upload');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//public` 注意这里  /img/ 前后必须都要有斜杠！！！
app.use('/upload/img', express.static(path.join(__dirname, '/upload/img')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/todos', todosRouter);
//jwt
// app.use('/article', AuthenticatePolicy.isValidToken, articleRouter);
app.use('/article', articleRouter);

app.use('/category', categoryRouter);
app.use('/comments', commentsRouter);
// app.use('/list', listRouter);
// app.use('/delall', delallRouter);
app.use('/dashboard', dashRouter);
app.use('/leavewords', leavewordsRouter);
app.use('/upload', uploadRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
