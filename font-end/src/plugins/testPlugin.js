import Vue from 'vue'

//定义插件对象，有install方法。当调用Vue。use（）时，执行install方法
const testPlugin = {
  //当调用Vue.use()时，执行该方案
  install: (Vue) => {
    console.log('安装testPlugin插件')
    //一般给Vue的原型对象挂载一个变量
    Vue.prototype.$http = {
      get: () => {
        console.log('发送get请求')
      },
    }
  },
}
Vue.use(testPlugin)
