//1.导入包 Vue axios实例对象
import Vue from 'vue'
import axios from '../request'
//2 定义插件对象
const xzdAxios = {
  install(Vue) {
    Vue.prototype.$http = axios
  },
}
//3 Vue.use()注册插件
Vue.use(xzdAxios)
