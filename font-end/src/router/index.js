import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/homepage',
    component: () => import('@/views/HomePage'),
    children: [
      {
        path: '/homepage',
        component: () => import('@/views/HomePage/HomePage'),
      },
      {
        path: '/fontArticle',
        component: () => import('@/views/fontArticle'),
      },
      {
        path: '/category',
        component: () => import('@/views/Category'),
      },
      {
        path: '/leavewords',
        component: () => import('@/views/Leavewords'),
      },
      {
        path: '/data',
        component: () => import('@/views/Data'),
      },
      {
        path: '/about',
        component: () => import('@/views/About'),
      },

      {
        path: '/fontDetail',
        component: () => import('@/views/fontDetail'),
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
