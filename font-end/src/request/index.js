//1 导入axios包
import axios from 'axios'
//2 实例化对象
const instance = axios.create({
  baseURL: 'http://122.112.251.104:3000',
  timeout: 3000,
})
//3 配置拦截器
instance.interceptors.response.use(
  //成功的回调
  (response) => {
    return response.data
  },
  //失败的回调
  (error) => {
    return Promise.reject(error)
  }
)
//4.导出实例对象
export default instance
