import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import './plugins/xzdAxios.js'
import * as echarts from 'echarts'

// markdown
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'


Vue.use(mavonEditor);
Vue.config.productionTip = false
Vue.prototype.$echarts = echarts

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app')
